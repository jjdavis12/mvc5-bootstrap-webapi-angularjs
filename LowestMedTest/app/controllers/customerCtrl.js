﻿var ctrl = angular.module('LowestMedTest.CustomerController', []);

ctrl.controller('customerCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.model = {};

    $http.get('/api/customer').success(function (data) {
        $scope.model = data;
    });
}]);