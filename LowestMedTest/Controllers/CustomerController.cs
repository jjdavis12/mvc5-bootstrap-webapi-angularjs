﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataAccess;
using Newtonsoft.Json;

namespace LowestMedTest.Controllers
{
    public class CustomerController : ApiController
    {
        private JsonSerializerSettings jsonSerializerSettings =
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            };

        /// <summary>
        /// Get Customers Web API
        /// </summary>
        public IHttpActionResult GetCustomers()
        {
            IEnumerable<Customer> customers = null;

            using (var dbContext = new DataAccess.MedTestDBEntities())
            {
                dbContext.Configuration.LazyLoadingEnabled = false;
                customers = dbContext.Customers.Include("Orders").ToList();
            }

            return Json(customers, jsonSerializerSettings);
        }
    }
}
